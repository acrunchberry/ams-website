<body>
		<!-- start: Header -->
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</a>
				<a class="brand" href="index.php"><span>Admin Panel</span></a>
								
				<!-- start: Header Menu -->
				<div class="nav-no-collapse header-nav">
					<ul class="nav pull-right">
						
						<!-- start: User Dropdown -->
						 
		                 <li class="dropdown">
		                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['firstname'] . " " . $_SESSION['lastname'] ?><b class="caret"></b></a>
		                    <ul class="dropdown-menu">
		                        <li>
		                            <a href="profile.php"><i class="fa fa-fw fa-user"></i> Profile</a>
		                        </li>
		                       <li class="divider"></li>
		                        <li>
		                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
		                        </li>
		                    </ul>
		                </li>
			           	
						<!-- end: User Dropdown -->
					</ul>
				</div>
				<!-- end: Header Menu -->
				
			</div>
		</div>
	</div>