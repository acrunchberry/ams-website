<table class="table-hover table-striped table-bordered">
    <thead>
            <tr>
                <td>SKU</td>
                <td>Description</td>
                <td>Image</td>
                <td>Category ID</td>
                <td>Price</td>
                <td>Quantity</td>
            </tr>
    </thead>
    <tbody>
    <?php
    
    $query = "SELECT * FROM items";
    $select_items = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_assoc($select_items)) {
    $item_id = $row['item_id'];
    $item_sku = $row['item_sku'];
    $item_description = $row['item_description'];
    $item_image = $row['item_image'];
    $item_cat_id = $row['item_parent_id'];
    $item_price = $row['item_price'];
    $item_amount = $row['item_amount'];
    $item_parent_id = $row['item_parent_id'];
    
    echo "<tr>";
    echo "<td class='panel-body'>{$item_sku}</td>";
    echo "<td class='panel-body'>{$item_description}</td>";
    echo "<td class='panel-body'><a href='../images/{$item_image}' data-lightbox='{$item_sku}'><img width='100' src='../images/{$item_image}' alt=''></a></td>";
    
    $query = "SELECT * FROM categories WHERE cat_id = {$item_cat_id}";
    $select_parent_id = mysqli_query($connection, $query);
                                     
    while($row = mysqli_fetch_assoc($select_parent_id)) {
    $cat_id = $row['cat_id'];
    $cat_title = $row['cat_title'];
    echo "<td class='panel-body'>{$cat_title}</td>";
    }
    
    echo "<td class='panel-body'>{$item_price}</td>";
    echo "<td class='panel-body'>{$item_amount}</td>";
    echo "<td class='panel-body'><a href='items.php?source=edit_item&item_id={$item_id}'>Edit</a></td>";
    echo "<td class='panel-body'><a href='items.php?delete={$item_id}'>Delete</a></td>";
    echo "</tr>";
    }

?>
    </tbody>
</table>

<?php

if(isset($_GET['delete'])) {
$the_item_id = $_GET['delete'];

$query = "DELETE FROM items WHERE item_id = {$the_item_id} ";
$delete_query = mysqli_query($connection, $query);
header("Location: ../items.php");
} 
?>