<?php

if(isset($_GET['item_id'])) {

    $the_item_id = $_GET['item_id'];    
    
}

$query = "SELECT * FROM items WHERE item_id = $the_item_id ";
$select_items_by_id = mysqli_query($connection, $query);
    
while($row = mysqli_fetch_assoc($select_items_by_id)) {
    $item_id = $row['item_id'];
    $item_sku = $row['item_sku'];
    $item_description = $row['item_description'];
    $item_image = $row['item_image'];
    $item_price = $row['item_price'];
    $item_amount = $row['item_amount'];
    $item_parent_id = $row['item_parent_id'];
    
    }
    
    if(isset($_POST['update_item'])) {
    
    $item_sku = $_POST['item_sku'];
    $item_description = $_POST['item_description'];
    $item_image = $_FILES['image']['name'];
    $item_image_temp = $_FILES['image']['tmp_name'];
    $item_price = $_POST['item_price'];
    $item_amount = $_POST['item_amount'];
    $item_parent_id = $_POST['item_parent_id'];
    
    move_uploaded_file($item_image_temp, "../images/$item_image");
    
    if(empty($item_image)) {
        
    $query = "SELECT * FROM items WHERE item_id = $the_item_id ";
    $select_image = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_array($select_image))
        {
        $item_image = $row['item_image'];
        }
    }
    
    $query = "UPDATE items SET ";
    $query .= "item_sku = '{$item_sku}', ";
    $query .= "item_description = '{$item_description}', ";
    $query .= "item_image = '{$item_image}', ";
    $query .= "item_price = '{$item_price}', ";
    $query .= "item_amount = '{$item_amount}', ";
    $query .= "item_parent_id = '{$item_parent_id}' ";
    $query .= "WHERE item_id = {$the_item_id} ";
    
    $update_item = mysqli_query($connection, $query);
    
    confirmQuery($update_item);
    
    echo "<p class='bg-success'>Item Updated. <a href='../item.php?item_id={$the_item_id}'>View Item</a> or <a href='items.php'>Edit More Items</a> ";
    }
?>

<form action="" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="sku">Item SKU</label>
            <input value="<?php echo $item_sku; ?>" type="text" class="form-control" name="item_sku">
    </div>
    
    <div class="form-group">
        <label for="content">Item Description</label>
            <textarea class="form-control" name="item_description" id="" cols="30" rows="10">
                <?php echo $item_description; ?>
            </textarea>
    </div>
    
    <div class="form-group" name="item_image">
        <label for="image">Item Image</label>
        </br>
            <img width="" src='../images/<?php echo "{$item_image}"; ?>' alt="">
            <input type="file" name="image">
    </div>
    
    <div class="form-group">
        <label for="price">Item Price</label>
            <select name="item_amount" id="">
                
            </select>
            <input value="<?php echo $item_price; ?>" type="text" class="form-control" name="item_price">
    </div>
    <div class="form-group">
        <label for="amount">Item Amount</label>
            <input value="<?php echo $item_amount;?>" type="text" class="form-control" name="item_amount">
    </div>
    
    <div class="form-group">
        <label for="item_parent_id">Item Category</label>
        </br>
        <select name="item_parent_id" id="">
        <?php
        
        $query = "SELECT * FROM categories";
        $select_categories = mysqli_query($connection, $query);
        
        confirmQuery($select_categories);
            
        while($row = mysqli_fetch_assoc($select_categories)) {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
            
            echo "<option value='$cat_id'>{$cat_title}</option>";
            }
        
        ?>
        </select>
    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="update_item" value="Update Item">
    </div>
</form>