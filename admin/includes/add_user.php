<?php

if(isset($_POST['create_user'])) {
    
$user_firstname = $_POST['user_firstname'];
$user_lastname = $_POST['user_lastname'];
$user_role = $_POST['user_role'];
$username = $_POST['username'];
$user_email = $_POST['user_email'];
$user_password = $_POST['user_password'];

$query = "INSERT INTO users(user_firstname, user_lastname, user_role, username, user_email, user_password) ";
$query .= "VALUES('{$user_firstname}','{$user_lastname}','{$user_role}','{$username}','{$user_email}', '{$user_password}' ) ";    
$create_user_query = mysqli_query($connection, $query);
confirmQuery($create_user_query);

echo "<div class='alert alert-success'>User Added!</div>" . " " . "<a href='users.php>View Users</a>";
header("Location: users.php");
}

?>



<form action="" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="user_firstname">First Name</label>
            <input type="text" class="form-control" name="user_firstname">
    </div>
    
    <div class="form-group">
        <label for="user_lastname">Last Name</label>
            <input type="text" class="form-control" name="user_lastname">
    </div>

     <div class="form-group">
        <label for="user_role">User Role</label>
        </br>
        <select name="user_role" id="">
            <option value="subscriber">Select Options</option>
            <option value="super_admin">Super-Admin</option>
            <option value="admin">Admin</option>
            <option value="customer">Customer</option>
        
        </select>
    </div>
     <div class="form-group">
        <label for="username">Username</label>
            <input type="text" class="form-control" name="username">
    </div>
    
    <div class="form-group">
        <label for="user_email">User Email</label>
            <input type="email" class="form-control" name="user_email">
    </div>
    <div class="form-group">
        <label for="user_password">User Password</label>
            <input type="password" class="form-control" name="user_password">
    </div>
    
    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_user" value="Add User">
    </div>
</form>