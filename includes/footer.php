			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<footer id="footer" class="clearfix ">

				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<div class="footer-content text-center padding-ver-clear">
										<div class="logo-footer"><img id="logo-footer" class="center-block" src="images/ams-logo.png" alt="American Mobile Source"></div>
										<p>American Mobile Source will purchase your smartphone LCD assemblies, "cores", and will pay you fair market value based on the quality of the item.</p>
										<ul class="list-inline mb-20">
											<li><i class="text-default fa fa-map-marker pr-5"></i>San Antonio, TX US</li>
											<li class="link-dark"><i class="text-default fa fa-phone pl-10 pr-5"></i>Contact Us: +1(877) 389-0838</li>
											<li class="link-dark"><i class="text-default fa fa-envelope-o pl-10 pr-5"></i>sales@americanmobilesource.com</li>
										</ul>
										<div class="separator"></div>
										<p class="text-center margin-clear">Copyright © 2015 American Mobile Source. All Rights Reserved</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

			</footer>
			<!-- footer end -->
			
		</div>
		<!-- page-wrapper end -->
<?php include "includes/jscripts.php"; ?>

	</body>
</html>