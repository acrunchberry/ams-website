                        <!-- banner start -->
            			<!-- ================ -->
            			<div id="banner" class="banner dark-translucent-bg padding-bottom-clear" style="background-image:url('images/lab-techs.png');">
            				<!-- section start -->
            				<!-- ================ -->
            				<div class="container">
            					<div class="row>">
            						<div class="col-md-8 text-center col-md-offset-2 space-bottom">
            							<div class="title large_white">American Mobile Source<br><h5>Quality from the Source!</h5></div>
            							<div class="separator"></div>
            							<p class="text-center lead">American Mobile Source will purchase your smartphone LCD assemblies, "cores", and will pay you fair market value based on the quality of the item.</p>
            						</div>
            					</div>
            					<div class="row mb-20">
            						<div class="col-md-4">
            							<div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
            								<span class="icon default-bg small"><i class="icon-eye"></i></span>
            								<div class="body">
            									<h4 class="title">Quality Assurance</h4>
            									<p>
            										Our team makes all efforts to ensure you receive the highest quality parts. From the moment we
            										receive your LCD "cores" to the time we begin the remanufacturing process. We ensure our processes
            										will minimize damage to the part to ensure you receive the highest quality remanufactured part.
            									</p>
            								</div>
            							</div>
            						</div>
            						<div class="col-md-4">
            							<div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="150">
            								<span class="icon default-bg small"><i class="icon-trophy"></i></span>
            								<div class="body">
            									<h4 class="title">Favorable Payouts</h4>
            									<p>
            										Our QA process allow us to provide you with the highest fair market value of your LCD "core" screen.
            										We are able to process large volumes of screen "cores" which helps you maximize your payout in as a 
            										short time as possible. Ask us about our volume purchase rates!
            									</p>
            								</div>
            							</div>
            						</div>
            						<div class="col-md-4">
            							<div class="feature-box-2 object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="200">
            								<span class="icon default-bg small"><i class="icon-flag"></i></span>
            								<div class="body">
            									<h4 class="title">US Facility</h4>
            									<p>
            										We are a remanufacturing facility based in the US that can meet all your parts need instead of having
            										to import or order parts from China. Ordering from American Mobile Source allows for faster shipping
            										while maintaing the highest part quality at the lowest price. No longer rely on cheap parts from China
            										and be unsure of the quality.
            									</p>
            								</div>
            							</div>
            						</div>
            					</div>					
            				</div>
            				<!-- section end -->
            			</div>
            			<!-- banner end -->
