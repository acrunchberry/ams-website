<div class="bs-exampler">
    <div class="row">
        <table class="table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <td>SKU</td>
                    <td>Description</td>
                    <td>Image</td>
                    <td>Category ID</td>
                    <td>Price</td>
                    <td>Quantity</td>
                    <td>Order</td>
                </tr>
            </thead>
            <tbody>
                    <?php
    
    $query = "SELECT * FROM items WHERE item_parent_id = 3";
    $select_items = mysqli_query($connection, $query);
    
    while($row = mysqli_fetch_assoc($select_items )) {
    $item_id = $row['item_id'];
    $item_sku = $row['item_sku'];
    $item_description = $row['item_description'];
    $item_image = $row['item_image'];
    $item_cat_id = $row['item_parent_id'];
    $item_price = $row['item_price'];
    $item_amount = $row['item_amount'];
    $item_parent_id = $row['item_parent_id'];
    
    echo "<tr>";
    echo "<td class='panel-body'>{$item_sku}</td>";
    echo "<td class='panel-body'>{$item_description}</td>";
    echo "<td class='panel-body'><a href='../images/{$item_image}' data-lightbox='{$item_sku}'><img width='100' src='../images/{$item_image}' alt=''></a></td>";
    
    $query = "SELECT * FROM categories WHERE cat_id = {$item_cat_id}";
    $select_parent_id = mysqli_query($connection, $query);
                                     
    while($row = mysqli_fetch_assoc($select_parent_id)) {
    $cat_id = $row['cat_id'];
    $cat_title = $row['cat_title'];
    echo "<td class='panel-body'>{$cat_title}</td>";
    }
    
    echo "<td class='panel-body'>{$item_price}</td>";
    echo "<td class='panel-body'><select name='' id=''><option value=''></option></select>
          </td>";
    
    echo "<td class='panel-body'><button class='btn btn-primary'>Add to Cart</button></td>";
    echo "</tr>";
    }
?>
            </tbody>
        </table>
    </div>
</div>