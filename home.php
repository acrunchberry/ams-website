<?php include "includes/header.php"; ?>

	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<?php include "includes/contact-top.php"; ?>
			<?php include "includes/navbar-top.php"; ?>
			</div>
			<!-- header-container end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<?php include "banner-top.php"; ?>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<?php include "page-1.php"; ?>
						<!-- main end -->

					</div>
				</div>
			</section>
			<!-- main-container end -->
	
			
			<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<?php include "includes/footer.php";  ?>