				<div class="header-top colored ">
					<div class="container">
						<div class="row">
							<div class="col-xs-3 col-sm-6 col-md-9">
								<!-- header-top-first start -->
								<!-- ================ -->
								<div class="header-top-first clearfix">
									<ul class="social-links circle small clearfix hidden-xs">
										<li class="googleplus"><a target="_blank" href="https://plus.google.com/104382270718037909604"><i class="fa fa-google-plus"></i></a></li>
										<li class="youtube"><a target="_blank" href="https://www.youtube.com/watch?v=fD884cAtDWo"><i class="fa fa-youtube-play"></i></a></li>
										<li class="facebook"><a target="_blank" href="https://www.facebook.com/American-Mobile-Source-1701957136683935"><i class="fa fa-facebook"></i></a></li>
								    </ul>
								    <div class="social-links hidden-lg hidden-md hidden-sm circle small">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-share-alt"></i></button>
											<ul class="dropdown-menu dropdown-animation">
												<li class="googleplus"><a target="_blank" href="https://plus.google.com/104382270718037909604"><i class="fa fa-google-plus"></i></a></li>
												<li class="youtube"><a target="_blank" href="https://www.youtube.com/watch?v=fD884cAtDWo"><i class="fa fa-youtube-play"></i></a></li>
												<li class="facebook"><a target="_blank" href="https://www.facebook.com/American-Mobile-Source-1701957136683935"><i class="fa fa-facebook"></i></a></li>
											</ul>
										</div>
									</div>
									<ul class="list-inline hidden-sm hidden-xs">
										<li><i class="fa fa-phone pr-5 pl-10"></i>+1 877-389-0838</li>
										<li><i class="fa fa-envelope-o pr-5 pl-10"></i> sales@americanmobilesource.com</li>
									</ul>
								</div>
								<!-- header-top-first end -->
							
							</div>
						</div>
					</div>
				</div>
				<!-- header-top end -->
