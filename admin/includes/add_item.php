<?php

if(isset($_POST['create_item'])) {
    
$item_sku = $_POST['item_sku'];
$item_description = $_POST['item_description'];
$item_image = $_FILES['image']['name'];
$item_image_temp = $_FILES['image']['tmp_name'];

$item_price = $_POST['item_price'];
$item_amount = $_POST['item_amount'];
$item_parent_id = $_POST['item_parent_id'];

move_uploaded_file($item_image_temp, "../images/$item_image" );

$query = "INSERT INTO items(item_sku, item_description, item_image, item_price, item_amount, item_parent_id) ";
$query .= "VALUES({$item_sku},'{$item_description}','{$item_image}','{$item_price}','{$item_amount}', '{$item_parent_id}' ) ";    

$create_item_query = mysqli_query($connection, $query);

confirmQuery($create_item_query);
echo "<p class='alert alert-success'>Item Added. <a href='./item.php?p_id={$the_item_id}'>View Item</a> or <a href='items.php'>Edit More Items</a> ";
}

?>

<form action="" method="post" enctype="multipart/form-data">
    
    <div class="form-group">
        <label for="sku">Item SKU</label>
            <input type="text" class="form-control" name="item_sku">
    </div>
    
    <div class="form-group">
        <label for="content">Item Description</label>
            <textarea class="form-control" name="item_description" id="" cols="30" rows="10"></textarea>
    </div>
    
    <div class="form-group">
        <label for="image">Item Image</label>
            <input type="file" name="image">
    </div>
    
    <div class="form-group">
        <label for="price">Item Price</label>
            <input type="text" class="form-control" name="item_price">
    </div>
    <div class="form-group">
        <label for="amount">Item Amount</label>
            <input type="text" class="form-control" name="item_amount">
    </div>
    <div class="form-group">
        <label for="item_parent_id">Item Category</label>
        </br>
        <select name="item_parent_id" id="">
        <?php
        
        $query = "SELECT * FROM categories";
        $select_categories = mysqli_query($connection, $query);
        
        confirmQuery($select_categories);
            
        while($row = mysqli_fetch_assoc($select_categories)) {
        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];
            
            echo "<option value='$cat_id'>{$cat_title}</option>";
            }
        
        ?>
        </select>
    </div>

    <div class="form-group">
        <input class="btn btn-primary" type="submit" name="create_item" value="Add Item">
    </div>
</form>