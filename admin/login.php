<?php include "includes/db.php"; ?>
<?php session_start(); ?>
<?php

if(isset($_POST['login'])) {
	
$username = $_POST['username'];
$password = $_POST['password'];

$username = mysqli_real_escape_string($connection, $username);    
$password = mysqli_real_escape_string($connection, $password);

$query = "SELECT * FROM users WHERE username = '{$username}' ";
$login_query = mysqli_query($connection, $query);
if(!$login_query) {
    die("QUERY FAILED" . mysqli_error($connection));
}
while($row = mysqli_fetch_array($login_query)) {
    $login_id = $row['user_id'];
    $login_username = $row['username'];
    $login_password = $row['user_password'];
    $login_firstname = $row['user_firstname'];
    $login_lastname = $row['user_lastname'];
    $login_role = $row['user_role'];
}

if($username !== $login_username && $password !== $login_password) {
	header("Location: login_error.php");
} else {
	
$_SESSION['username'] = $login_username;
$_SESSION['firstname'] = $login_firstname;
$_SESSION['lastname'] = $login_lastname;
$_SESSION['user_role'] = $login_role;
	
	header("Location: dashboard.php");
}


}

?>

<?php include "includes/admin_login_header.php"; ?>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<a href="index.php"><i class="halflings-icon home"></i></a>
						<a href="#"><i class="halflings-icon cog"></i></a>
					</div>
					<h2>Login to your account</h2>
					<form class="form-horizontal" action="login.php" method="post">
						<fieldset>
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="username" id="username" type="text" placeholder="type username"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
							</div>
							<div class="clearfix"></div>
							
							<!--<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>-->

							<div class="button-login">	
								 <button class="btn btn-primary" name="login" type="submit">Login</button>
							</div>
							<div class="clearfix"></div>
					</form>
					<hr>
					<!--<h3>Forgot Password?</h3>-->
					<!--<p>-->
					<!--	No problem, <a href="#">click here</a> to get a new password.-->
					<!--</p>	-->
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	
<?php include "includes/admin_login_footer.php"; ?>