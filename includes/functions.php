<?php include "includes/db.php"; ?>
<?php
// Function to loop menu array
function loop_array($array = array(), $parent_id = 0) 
{
	if(!empty($array[$parent_id])) {
	    echo "<ul class='dropdown-menu'>";
		foreach ($array[$parent_id] as $items) {
		    
			echo "<li class='dropdown'><a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>";
			echo $items['menu_name'];
			loop_array($array, $items['menu_id']); 
			echo "</a></li>";
			
		}
		echo "</ul>";
	}
}

function display_menus(){
global $connection;

	$query = "SELECT * FROM menus";
    $select_categories = mysqli_query($connection, $query);
    
	$array = array();
	
	if (mysqli_num_rows($select_categories)) {
		while ($rows = mysqli_fetch_array($select_categories)) {
		$array[$rows['parent_id']][] = $rows;
			    $menu_id = $row['menu_id'];
			    $menu_name = $row['menu_name'];
			    $parent_id = $row['parent_id'];
			
		}
		
		loop_array($array); 
	}
	
}

?>