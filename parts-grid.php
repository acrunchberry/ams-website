<!-- Icon Boxes start -->
<!-- ================ -->
	
<!-- section -->
<!-- ================ -->
<section class="padding-bottom-clear">
	<div class="main col-md-12">
		<div class="row">
			<div class="col-md-4">
				<div class="pv-30 ph-20 feature-box bordered shadow text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<span class="icon default-bg circle"><i class="fa fa-mobile"></i></span>
					<h3>iPhone 5 Parts</h3>
					<a class="btn btn-danger" href="iphone-5.php">Click Here</a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pv-30 ph-20 feature-box bordered shadow text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<span class="icon default-bg circle"><i class="fa fa-mobile"></i></span>
					<h3>iPhone 5c Parts</h3>
					<a class="btn btn-danger" href="iphone-5c.php">Click Here</a>
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="pv-30 ph-20 feature-box bordered shadow text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
					<span class="icon default-bg circle"><i class="fa fa-mobile"></i></span>
					<h3>iPhone 5s Parts</h3>
					<a class="btn btn-danger" href="iphone-5s.php">Click Here</a>
				</div>
			</div>
		</div>
		<br>

	
	</div>
</section>
<!-- section end -->

